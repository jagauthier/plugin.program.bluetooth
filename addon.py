import os
import xbmc
import xbmcgui
import xbmcaddon
import threading
import time
import subprocess
import dbus
import dbus.service

###################################################################################################
###################################################################################################
# Initialization
###################################################################################################
###################################################################################################
ACTION_PREVIOUS_MENU = 10
ACTION_SELECT_ITEM = 7

TEXT_ALIGN_LEFT = 0
TEXT_ALIGN_RIGHT = 1
TEXT_ALIGN_CENTER_X = 2
TEXT_ALIGN_CENTER_Y = 4
TEXT_ALIGN_RIGHT_CENTER_Y = 5
TEXT_ALIGN_LEFT_CENTER_X_CENTER_Y = 6

SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'


def log(logline):
    print "BLUEGUI: " + logline
    
def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")
    return value


# Get global paths
addon = xbmcaddon.Addon(id = "plugin.program.bluetooth")
resourcesPath = os.path.join(addon.getAddonInfo('path'),'resources') + '/'
srcPath = os.path.join(addon.getAddonInfo('path'),'src') + '/'
mediaPath = os.path.join(addon.getAddonInfo('path'),'resources','media') + '/'

carPCskin = xbmcaddon.Addon(id = "skin.CarPC-touch_carbon")
carPCbg=os.path.join(carPCskin.getAddonInfo('path'),'background') + '/'

addonW = 1280
addonH = 720

mac_list = []
bt_path_list = []

statusbar = xbmcgui.ControlLabel(50,650,addonW,100,'',textColor='0xffffffff',font='font24_title')
pairlabel = xbmcgui.ControlLabel(125,400,addonW,100,'',textColor='0xffffffff',font='font24_title')

#Visible
button_onoff = xbmcgui.ControlRadioButton(50, 100, 400, 80, 
                                                    label="Bluetooth",
                                                    focusTexture = mediaPath + "right.png",
                                                    noFocusTexture = mediaPath + "right.png",
                                                    textOffsetX = 40,
                                                    font='font30_title',
                                                    textColor='0xFFFFFFFF',
                                                    focusOnTexture = mediaPath + "radiobutton-focus.png", 
                                                    noFocusOnTexture = mediaPath + "radiobutton-focus.png",
                                                    focusOffTexture = mediaPath + "radiobutton-nofocus.png", 
                                                    noFocusOffTexture = mediaPath + "radiobutton-nofocus.png")

button_visible = xbmcgui.ControlRadioButton(50, 200, 400, 80, 
                                                    label="Discoverable",
                                                    focusTexture = mediaPath + "right.png",
                                                    noFocusTexture = mediaPath + "right.png",
                                                    textOffsetX = 40,
                                                    font='font30_title',
                                                    textColor='0xFFFFFFFF',
                                                    focusOnTexture = mediaPath + "radiobutton-focus.png", 
                                                    noFocusOnTexture = mediaPath + "radiobutton-focus.png",
                                                    focusOffTexture = mediaPath + "radiobutton-nofocus.png", 
                                                    noFocusOffTexture = mediaPath + "radiobutton-nofocus.png")
                                                    
button_pairable = xbmcgui.ControlRadioButton(50, 300, 400, 80,
                                                    label="Pairable",
                                                    focusTexture = mediaPath + "right.png",
                                                    noFocusTexture = mediaPath + "right.png",
                                                    textOffsetX = 40,
                                                    font='font30_title',
                                                    textColor='0xFFFFFFFF',
                                                    focusOnTexture = mediaPath + "radiobutton-focus.png", 
                                                    noFocusOnTexture = mediaPath + "radiobutton-focus.png",
                                                    focusOffTexture = mediaPath + "radiobutton-nofocus.png", 
                                                    noFocusOffTexture = mediaPath + "radiobutton-nofocus.png")


paired_list = xbmcgui.ControlList(600, 150, 500, 500,textColor='0xffffffff',font='font28_title', selectedColor="0x00000000", _space=45)


def get_btdevices():
    paired_list.reset();
    del mac_list[:]
    del bt_path_list[:]    
    
    paired_devices=int(get_kodi_prop("paired_devices"))
    i=1
    while i<=paired_devices:
        paired_list.addItem(get_kodi_prop("bt_device" + str(i)))
        mac_list.append(get_kodi_prop("bt_mac" + str(i)))
        bt_path_list.append(get_kodi_prop("bt_devpath" + str(i)))
        i+=1

class updateStatus(threading.Thread):
    def run(self):
        self.shutdown = False
        old_bt=0

        informed=False
        while not self.shutdown:
            # check connected status - covers connections that happen prior to the addon running
            # and connections made outside (from the other device)
            bluetooth.pair_name=get_kodi_prop("connected_device")
            if bluetooth.pair_name!="":
                statusbar.setLabel("Status: Connected to " + bluetooth.pair_name)
                bluetooth.button_bt.setVisible(True)
                bluetooth.btimage.setVisible(True)
            else:
                statusbar.setLabel("Status: Not connected")
                bluetooth.button_bt.setVisible(False)
                bluetooth.btimage.setVisible(False)
            
            powered=int(get_kodi_prop('bt_power'))
            bt_avail=int(get_kodi_prop('bt_avail'))
            discoverable=int(get_kodi_prop('bt_discoverable'))
            pairable=int(get_kodi_prop('bt_pairable'))
            
            if bt_avail==1 and old_bt==0:
                button_onoff.setEnabled(True)
                button_visible.setEnabled(True)
                button_pairable.setEnabled(True)
                paired_list.setEnabled(True)
                
            old_bt=bt_avail
            if bt_avail==0:
                button_onoff.setEnabled(False)
                button_visible.setEnabled(False)
                button_pairable.setEnabled(False)
                paired_list.setEnabled(False)
                paired_list.reset();
                del mac_list[:]
                statusbar.setLabel("Status: No bluetooth devices.")
                if informed==False:
                    xbmcgui.Dialog().ok("Device missing","No bluetooth devices are available.")
                    informed=True
                time.sleep(2)
                continue
            
            if powered:
                button_onoff.setSelected(True)
            else:
                button_onoff.setSelected(False)
            
            if pairable:
                button_pairable.setSelected(True)
            else:                
                button_pairable.setSelected(False)
                
            if discoverable:
                button_visible.setSelected(True)
            else:
                button_visible.setSelected(False)
                
            if self.shutdown == True:
                self.close()

            get_btdevices()
            time.sleep(2)

class bluetooth(xbmcgui.WindowDialog):
    
    bt_device=None
    pair_name=""
    button_bt=None 
    btimag=None
    def __init__(self):
        

        self.w = addonW
        self.h = addonH
        # Background        
        self.background=xbmcgui.ControlImage(0, 0, self.w, self.h, carPCbg + "SKINDEFAULT.jpg")
        self.addControl(self.background)
        # top and bottom images
        self.addControl(xbmcgui.ControlImage(0, 0, self.w, 90, mediaPath + "top_bar.png"))
        self.addControl(xbmcgui.ControlImage(0, self.h - 90, self.w, 90, mediaPath + "bottom_bar.png"))
        #TITLE
        self.addControl(xbmcgui.ControlLabel(
            0, 25,
            self.w, 100,
            'Bluetooth Settings',
            textColor='0xffffffff',
            font='font30_title',
            alignment=TEXT_ALIGN_CENTER_X))

        #Visibility
        self.addControl(button_visible)
        button_visible.setRadioDimension(270,0, width=100, height=100)
        self.addControl(button_onoff)
        button_onoff.setRadioDimension(270,0, width=100, height=100)
        self.addControl(button_pairable)
        button_pairable.setRadioDimension(270,0, width=100, height=100)        

        # text areas
        self.addControl(xbmcgui.ControlLabel(600, 100,self.w, 100,'Paired Devices:',textColor='0xffffffff',font='font30_title'))

        # statusbar
        self.addControl(statusbar)        
        
        # back button
        self.button_back=xbmcgui.ControlButton(0, 0, 83, 83, "", "floor_buttonFO.png", "floor_button.png", 0, 0)
        self.addControl(self.button_back)
        self.addControl(xbmcgui.ControlImage(0, 0, 83, 83, "icon_back_w.png"))

        # bluetooth button        
        bluetooth.button_bt=xbmcgui.ControlButton(self.w-100, self.h-83, 83, 83, "", "floor_buttonFO.png", "floor_button.png", 0, 0)
        self.addControl(bluetooth.button_bt)
        bluetooth.btimage=xbmcgui.ControlImage(self.w - 100, self.h - 83, 83, 83, mediaPath + "bluetooth_cancel.png")
        self.addControl(bluetooth.btimage)

        bluetooth.button_bt.setVisible(False)
        bluetooth.btimage.setVisible(False)

        # do dbus connectivity
        self.bus = dbus.SystemBus()
        self.manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = self.manager.GetManagedObjects()
        
        for path, interfaces in objects.iteritems():
            if ADAPTER_IFACE in interfaces:
                self.adapter=self.bus.get_object("org.bluez", path)
                self.adapter_manager=dbus.Interface(self.adapter, 'org.freedesktop.DBus.Properties')
                
        self.addControl(paired_list)
        
        pairlabel.setLabel("Pairing code: " + get_kodi_prop("pairing_code"))
        self.addControl(pairlabel)

        self.updateThread = updateStatus()
        self.updateThread.start()        
                
        
    def onControl(self, controlID):
        
        print "CONTROL"
        print controlID
        # discoverable Button
        if controlID == button_visible:            
                if button_visible.isSelected() :
                    self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', True)
                else:                    
                    self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', True)

        if controlID == button_onoff:
                if button_onoff.isSelected() :
                    self.adapter_manager.Set(ADAPTER_IFACE, 'Powered', True)
                else:                    
                    self.adapter_manager.Set(ADAPTER_IFACE, 'Powered', True)
 
        if controlID == button_pairable:            
                if button_pairable.isSelected() :
                    subprocess.call("echo 'pairable yes' | sudo bluetoothctl", shell=True)
                else:                    
                    subprocess.call("echo 'pairable no' | sudo bluetoothctl", shell=True)                    

        if controlID == paired_list: 
                # we subtract one here, because this all starts at 1, not zero.
                position=paired_list.getSelectedPosition()
                log("Selected position: " + str(position))
                log("bt_path: " + bt_path_list[position])
                log("bt_mac: " + mac_list[position])
                log("item: " + paired_list.getSelectedItem().getLabel())
                device_name=paired_list.getSelectedItem().getLabel()
                device=self.bus.get_object("org.bluez", bt_path_list[position])
                device_properties=device.GetAll(DEVICE_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                device_manager=dbus.Interface(device, DEVICE_IFACE)                
                if (int(device_properties["Connected"]))==0:
                    connect=xbmcgui.Dialog().yesno(device_name,"Please choose to connect or unpair the device.",yeslabel="Connect",nolabel="Unpair")
                    if connect:
                        if bt_path_list[position]:
                            log("Connecting to: " + bt_path_list[position])
                            try:
                                statusbar.setLabel("Status: Connecting to " +device_name)
                                device_manager.Connect()
                            except:
                                xbmcgui.Dialog().ok(device_name,"Connection failed.")
                                return
                        bluetooth.pair_name=device_name
                    else:
                        ### unpair code - can't figure out how to do this from dbus :(
                        bt_disc="sudo bt-device -r " + mac_list[paired_list.getSelectedPosition()]
                        subprocess.call(bt_disc, shell=True)
 
                else:
                    device_manager.Disconnect()
                    xbmcgui.Dialog().ok(device_name,"Device disconnected.")
                    
        # Back button
            
        if controlID == self.button_back:
            log("ENDING.")            
            self.shutdown = True
            self.updateThread.shutdown = True
            self.updateThread.join()
            strWndFnc = "XBMC.ActivateWindow(10000)"
#            xbmc.executebuiltin(strWndFnc)          
            log("ENDING.")
            self.close()            
        # bluetooth cancel button
        if controlID == bluetooth.button_bt:
            connected_path=get_kodi_prop("connected_path")
            if connected_path!="":
                device=self.bus.get_object("org.bluez", connected_path)
                device_properties=device.GetAll(DEVICE_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                device_manager=dbus.Interface(device, DEVICE_IFACE)
                device_manager.Disconnect()


# Start the Addon

dialog = bluetooth()
dialog.doModal()
del dialog
del addon
